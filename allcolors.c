#include <stdio.h>

int main(){

	printf("This script will display all possible RGB values supported by a true color ternimal. It will flash many colors very quickly. For best results, your ternimal width should be at least 256 characters wide. Press the enter key to continue.\n");
	scanf("Press the enter key to continue");
	int i = 0;
	int j = 0;
	int k = 0;

	for(i; i<256; i++){
		for(j; j<256; j++){
			for(k; k<256; k++){
				printf("\033[48:2:%i:%i:%im \033[0m", i, j, k);

				if(k==255){
					printf("\n");
				}

			}
			k = 0;
		}
		j = 0;
	}

	printf("\n");
	return 0;
}

